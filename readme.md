
***Availability:***


| Image  | Image Name | Usage                                                 |
|--------|------------|-------------------------------------------------------|
| Apache | apache     | Self-sufficient for usage with most PHP applications. |
| FPM    | fpm        | PHP Only container                                    |
| NGINX  | nginx      | Self-sufficient for usage with most PHP applications  |




***example:***
```bash
docker build -t hareland/php-image:apache-1.0 -f ./src/apache.dockerfile .
docker push hareland/php-image:apache-1.0
```


Example:
```dockerfile
FROM hareland/image-php:apache-1.0

RUN docker-ext-install zip bcmath #....

```