#! /usr/bin/env python
import sys

try:
    input = sys.argv[1]
except IndexError as e:
    input = ''

print("{}".format(input).replace('-', '_').replace('/', '_').replace('.', '_').replace('\\', '_'))