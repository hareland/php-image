#! /usr/bin/env python
import json
import os
import sys

from helpers import json_to_env

file = sys.argv[1]
outfile = sys.argv[2]

# Write json to .env file
with open(outfile, 'w') as op:
    op.write(json_to_env(file))
