#! /usr/bin/env python
import decimal
import json
import numbers
import sys


def is_string(val):
    # Check if python 2 or 3
    if sys.version_info[0] == 3:
        return isinstance(val, str)
    else:
        return isinstance(val, basestring)


def is_numeric(val):
    if isinstance(val, int) or isinstance(val, float):
        return True

    return False


def is_boolean(val):
    if isinstance(val, bool):
        return True

    return False


def quote_count_limit_reached(quote_count, char):
    return quote_count[char] and is_numeric(quote_count[char]) and quote_count[char] > 2


def env_to_dict(env_file):
    """Parses a .env file to dict"""
    with open(env_file, 'r') as f:
        content = f.readlines()

    output = dict()

    # Parse each line
    for line in content:
        key = ""
        value = ""
        is_value = False
        quote_count = {}

        # Split on
        for char in list(line.strip()):
            # check if the char is " or ', then skip it..
            # TODO: Add support for escaped quotes in string...
            if char == '"' or char == "'":
                if quote_count[char]:
                    quote_count[char] += 1
                else:
                    quote_count[char] = 1

                if quote_count_limit_reached(quote_count, char):
                    raise Error('Quote limit reached for char: `{}`, current count: {}'.format(char, quote_count[char]))

                continue

            if char == '=':
                is_value = True
                continue

            # Key or value?
            if is_value:
                value += char
            else:
                key += char

        # Make sure to convert numbers!
        if not is_string(value):
            if isinstance(value, float):
                output[key] = float(value)
            elif isinstance(value, integer):
                output[key] = int(value)
            else:
                output[key] = value
        else:
            output[key] = value

    return output


def json_to_env(json_file=None):
    """Write .json to .env file"""
    output = ""
    with open(json_file, 'r') as p:
        dict = json.load(p)
        for k, v in dict.items():
            was_bool = False
            # Make sure that we convert booleans to lowercase chars
            if isinstance(v, bool):
                was_bool = True
                if v is True:
                    v = 'true'
                else:
                    v = 'false'

            # Make sure that we use correct variable type,
            # we should always quote strings that contain spaces!
            # Quotes are not valid when using kuvectl to create from .env file....
            # EXCEPT: if it was a boolean
            if is_string(v) and not was_bool and not is_numeric(v) and (" " in v):
                output += '{}="{}"\n'.format(k, v)
            else:
                output += '{}={}\n'.format(k, v)

    return output
