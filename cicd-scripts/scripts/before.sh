#!/usr/bin/env bash

# Pre-package stage

# If we did not set this, we should...
if [[ -z "$APP_ROOT" ]]; then
    APP_ROOT="/app"
    export APP_ROOT=$APP_ROOT
fi

# Directory required...
if [[ ! -d "$APP_ROOT" ]]; then
    mkdir $APP_ROOT
fi

# Directory required for nginx document root to not throw any errors
if [[ ! -d "$APP_ROOT/public" ]]; then
    echo "Could not find $APP_ROOT/public, creating folder now."
    mkdir $APP_ROOT/public
fi

# In order for logging from nginx to our app folder to work,
# we need this directory even if we dont yet have the application code.
if [[ ! -d "$APP_ROOT/storage/logs" ]]; then
    echo "Could not find $APP_ROOT/storage/logs, creating folder now."
    mkdir -p $APP_ROOT/storage/logs
fi

mkdir -p -m 0700 /root/.ssh

# Fix strict host keys...
echo "" > /root/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n" >> /root/.ssh/config

# Check for SSH usage
if [[ "$GIT_USE_SSH" == "1" ]] ; then
  echo -e "Host *\n\tUser ${GIT_USERNAME}\n\n" >> /root/.ssh/config
fi

exit 0