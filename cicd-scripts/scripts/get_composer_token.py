#! /usr/bin/env python
import sys
import json

try:
    token_key = sys.argv[1]
except IndexError as e:
    token_key = 'gitlab.com'
try:
    token_type = sys.argv[2]
except IndexError as e:
    token_type = 'gitlab-token'

try:
    auth_file = sys.argv[3]
except IndexError as e:
    auth_file = 'auth.json'

with open(auth_file, 'r') as auth_fp:
    auth = json.loads(auth_fp.read())
    try:
        print(auth[token_type][token_key])
    except IndexError as e:
        print('TOKEN NOT FOUND')
        exit(1)
