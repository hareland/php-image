#! /usr/bin/env python
import json
import re
import sys

try:
    file = sys.argv[1]
except IndexError as e:
    file = 'Dockerfile'

try:
    new_registry = sys.argv[2]
except IndexError as e:
    new_registry = 'registry.gitlab.com'

registry_match_regexp = 'FROM'
registry_replace_regexp = 'FROM ([a-zA-Z\.\:0-9]+)'

new_file = ""

with open(file, 'r') as fp:
    file_content = fp.readlines()
    # Find all line with FROM prefix and replace the registry URL
    for line in file_content:
        if re.search(registry_match_regexp, line):
            new_file += "{}\n".format(re.sub(registry_replace_regexp, "FROM {}".format(new_registry), line))
        else:
            new_file += line

print(new_file)