#! /usr/bin/env python
import json
import sys
from helpers import env_to_dict

"""Simple parser for parsing .env files with python and support quotes..."""

try:
    dotenv = sys.argv[1]
except IndexError as e:
    dotenv = '.env'

try:
    outputFile = sys.argv[2]
except IndexError as e:
    outputFile = 'env.json'

# Parse...
output = env_to_dict(dotenv)

# Write to output
with open(outputFile, 'w') as op:
    op.write(json.dumps(output))
