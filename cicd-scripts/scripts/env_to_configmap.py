#! /usr/bin/env python
import datetime
import json
import numbers
import os
import sys

from helpers import env_to_dict, is_numeric, is_boolean, is_string

# Data for the config map
config_map = dict()
config_map['apiVersion'] = 'v1'
config_map['resourceVersion'] = "5430115"

try:
    input_file = sys.argv[1]
except IndexError as e:
    input_file = '.env'

try:
    output_file = sys.argv[2]
except IndexError as e:
    output_file = 'configmap.yaml'

try:
    config_map['name'] = sys.argv[3]
except IndexError as e:
    config_map['name'] = 'default-name'

try:
    config_map['namespace'] = sys.argv[4]
except IndexError as e:
    config_map['namespace'] = 'default'

try:
    config_map['created'] = sys.argv[5]
except IndexError as e:
    today = datetime.date.today()
    config_map['created'] = '{}T{}Z'.format(today.strftime('%Y-%m-%d'), today.strftime('%H-%I-%S'))

try:
    template_file = sys.argv[6]
except IndexError as e:
    template_file = '{}/templates/configmap.tpl.yaml'.format(os.path.dirname(os.path.realpath(__file__)))


def dict_to_str(items, prefix=None):
    output = ''
    for k, v in items.items():
        if isinstance(v, numbers.Number) or is_numeric(v) or is_boolean(v):
            output += "{}{}: \"{}\"\n".format(prefix, k, v)
        else:
            output += '{}{}: {}\n'.format(prefix, k, v)

    return output


# Open .env file to dict
values = env_to_dict(input_file)

# Load template
with open(template_file, 'r') as template_fp:
    template_content = str(template_fp.read())
    template_content = template_content.replace('{{values}}', dict_to_str(values, '  '))
    for k, v in config_map.items():
        template_content = template_content.replace("{{"+'{}'.format(k) + "}}", v)

with open(output_file, 'w') as output_fp:
    output_fp.write(template_content)

print(template_content)
