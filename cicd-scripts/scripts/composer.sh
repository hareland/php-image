#!/usr/bin/env bash

BINARY_DIRECTORY="/usr/local/bin"

# If we did not set this, we should...
if [[ -z "$APP_ROOT" ]]; then
    APP_ROOT="/app"
    export APP_ROOT=$APP_ROOT
fi


# If composer is here, nothing to worry about
if [[ ! -f "$BINARY_DIRECTORY/composer" ]]; then
    echo "Installing composer..."
    cd /
    curl --silent --show-error https://getcomposer.org/installer | php \
        && mv /composer.phar $BINARY_DIRECTORY/composer

    # Assume it is not installed...
    echo "Adding hirak/prestissimo for quicker package downloads..."
    composer global require hirak/prestissimo
fi

# If this build comes with a composer.json file, we want to run composer install in the build process
if [[ -f "$APP_ROOT/composer.json" ]]; then
    # If we want to install packages, we should not have a vendor folder in our built image.
    if [[ ( -f "$APP_ROOT/vendor/autoload.php" ) && DOCKER_RUN_COMPOSER_INSTALL == 'yes' || ( -n "$FORCE_COMPOSER" ) ]]; then
        echo "Vendor folder already exists, skipping composer install"
    else
        # Look for composer key and set auth
        if [[ ( ! -z $COMPOSER_AUTH ) && ( $COMPOSER_AUTH != "" ) ]]; then
            echo "Adding \$COMPOSER_AUTH to $APP_ROOT/auth.json"
            echo $COMPOSER_AUTH > $APP_ROOT/$COMPOSER_AUTH_FILE

            # Import composer tokens with python
            composer config -g gitlab-token.gitlab.com $(python $APP_ROOT/cicd-scripts/scripts/get_composer_token.py 'gitlab.com' 'gitlab-token' $APP_ROOT/auth.json)

            # Make sure to remove the tokens...
            rm -f $APP_ROOT/auth.json
            export COMPOSER_AUTH=""
        else
            echo "NO COMPOSER AUTH???"
        fi

        echo "Installing Composer packages..."
        cd $APP_ROOT && composer install --ignore-platform-reqs --prefer-dist
    fi
fi
exit 0
