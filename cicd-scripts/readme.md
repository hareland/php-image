# CICD Scripts
Scripts to be re-used across deployments/images


***Scripts Included***
- .env to JSON transformer
- JSON to .env transformer
- Composer script
- Before script


***Script usage***

>`python scripts/env_to_json.py $IN $OUT`
```bash
python scripts/env_to_json.py .env env.json
```

>`python scripts/json_to_env.py $IN $OUT`
```bash
python scripts/json_to_env.py env.json .env
```

>`python scripts/update_docker_registry.py $DOCKERFILE $NEW_REGISTRY`
```bash
python scripts/update_docker_registry.py Dockerfile registry.domain.com
```

#### Before Script
Sets up different directories in the build environment, such as app directory logging directory etc.
this can be set using `$APP_ROOT`


### Composer script
Installs composer if required, installs hirak/prestissimo for parallel package installs (3-5x faster install),
Adds composer auth.json to the app directory if `$COMPOSER_AUTH` is set. this should be a JSON string