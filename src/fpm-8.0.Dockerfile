FROM devilbox/php-fpm-8.0:latest

COPY ./src /build
COPY ./cicd-scripts /build/cicd-scripts
RUN bash /build/cicd-scripts/scripts/before.sh


# Packages we think should be in all images
RUN echo "Installing required OS packages" \
    && apt-get update \
    && apt-get install -y \
        git \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
        libssl-dev \
        libz-dev \
        zlib1g-dev \
        libsqlite3-dev \
        zip \
        libxml2-dev \
        libcurl3-dev \
        libedit-dev \
        libpspell-dev \
        libldap2-dev \
        unixodbc-dev \
        libpq-dev \
        libbz2-dev \
        netcat \
        libzip-dev \
        nano

# https://bugs.php.net/bug.php?id=49876
RUN ln -fs /usr/lib/x86_64-linux-gnu/libldap.so /usr/lib/

# PHP Extensions
RUN echo "Installing PHP extensions" \
    && docker-php-ext-install -j$(nproc) gd pcntl pdo_mysql\
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-configure zip \
    && docker-php-ext-install zip \
    && curl -L -o /usr/local/bin/pickle https://github.com/FriendsOfPHP/pickle/releases/download/v0.6.0/pickle.phar \
    && chmod +x /usr/local/bin/pickle \
    && pickle install redis \
    && rm -rf /tmp/redis\
    && docker-php-ext-enable gd pcntl redis\
    && apt-get autoremove -y \
    && dpkg -la | awk '{print $2}' | grep '\-dev' | xargs apt-get remove -y \
    && apt-get clean all \
    && rm -rvf /var/lib/apt/lists/* \
    && rm -rvf /usr/share/doc /usr/share/man /usr/share/locale \
    && rm -rvf /usr/src/php

# install composer
RUN bash /build/cicd-scripts/scripts/composer.sh

EXPOSE 9000
