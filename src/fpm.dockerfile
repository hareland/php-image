FROM php:7.2-fpm

COPY ./src /build
COPY ./cicd-scripts /build/cicd-scripts
RUN bash /build/cicd-scripts/scripts/before.sh

# Packages we think should be in all images
RUN echo "Installing required OS packages" \
    && apt-get update \
    && apt-get install -y \
        git \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
        libssl-dev \
        libmemcached-dev \
        libz-dev \
        zlib1g-dev \
        libsqlite3-dev \
        zip \
        libxml2-dev \
        libcurl3-dev \
        libedit-dev \
        libpspell-dev \
        libldap2-dev \
        unixodbc-dev \
        libpq-dev \
        libbz2-dev \
        netcat \
        nano

# https://bugs.php.net/bug.php?id=49876
RUN ln -fs /usr/lib/x86_64-linux-gnu/libldap.so /usr/lib/

# PHP Extensions
RUN echo "Installing PHP extensions" \
    && docker-php-source extract \
    && docker-php-ext-install -j$(nproc) iconv gd mysql pdo_mysql pdo_pgsql pcntl pdo_sqlite zip curl bcmath opcache simplexml xmlrpc xml soap session readline pspell ldap mbstring bz2 \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && pecl install -o -f redis\
    && rm -rf /tmp/redis\
    && docker-php-ext-enable iconv gd mysql pdo_mysql pdo_pgsql pcntl pdo_sqlite zip curl bcmath opcache simplexml xmlrpc xml soap session readline pspell ldap mbstring redis\
    && git clone -b php7 https://github.com/php-memcached-dev/php-memcached.git /usr/src/php/ext/memcached \
    && cd /usr/src/php/ext/memcached \
    && git checkout origin/php7 \
    && docker-php-ext-configure memcached \
    && docker-php-ext-install memcached \
    && apt-get autoremove -y \
    && dpkg -la | awk '{print $2}' | grep '\-dev' | xargs apt-get remove -y \
    && apt-get clean all \
    && rm -rvf /var/lib/apt/lists/* \
    && rm -rvf /usr/share/doc /usr/share/man /usr/share/locale \
    && docker-php-source delete
#     && rm -rvf /usr/src/php

# install composer
RUN bash /build/cicd-scripts/scripts/composer.sh

EXPOSE 9000
