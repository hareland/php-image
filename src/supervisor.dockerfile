FROM registry.gitlab.com/hareland/php-image:nginx-latest

# Install supervisor
RUN apt-get update \
	&& apt-get install --no-install-recommends --no-install-suggests -y \
        ca-certificates \
        gettext-base \
        supervisor \
	&& rm -rf /var/lib/apt/lists/* \
	&& apt autoremove -y

# ..
EXPOSE 80


# .. Anything from the image you extend this from
