FROM registry.gitlab.com/hareland/php-image:fpm-latest
WORKDIR /app
MAINTAINER Kristian Hareland <khareland@workrate.eu>


# Fix fingeprint issue for external hosts
RUN ssh-keyscan -t rsa gitlab.workstate.net > ~/.ssh/known_hosts\
    && echo "Host gitlab.workstate.net\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config

RUN apt-get update -y \
    && apt-get install -y gcc

RUN yes | pecl install xdebug \
    && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini \
    && echo "xdebug.remote_autostart=off" >> /usr/local/etc/php/conf.d/xdebug.ini

