FROM alexcheng/magento2:2.2

RUN  curl -O https://download.libsodium.org/libsodium/releases/libsodium-1.0.18.tar.gz \
        && tar xfvz libsodium-1.0.18.tar.gz \
        && cd libsodium-1.0.18 \
        && ./configure \
        && make && make install \
        && pecl install -f libsodium \
        && echo 'extension=sodium.so' >> /usr/local/etc/php/conf.d/sodium.ini


# To make sure we have the correct timezone...
RUN apt-get update -y \
    && apt-get install -y tzdata \
    && export DEBCONF_NONINTERACTIVE_SEEN=true DEBIAN_FRONTEND=noninteractive \
    && echo "Europe/Oslo" > /etc/timezone \
    && dpkg-reconfigure tzdata \
    && export DEBIAN_FRONTEND=interactive DEBCONF_NONINTERACTIVE_SEEN=false

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*



