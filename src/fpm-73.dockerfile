FROM chialab/php:7.3-fpm


COPY ./src /build
COPY ./cicd-scripts /build/cicd-scripts
RUN bash /build/cicd-scripts/scripts/before.sh

# install composer
RUN bash /build/cicd-scripts/scripts/composer.sh

EXPOSE 9000
