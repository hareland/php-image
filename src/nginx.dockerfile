FROM registry.gitlab.com/hareland/php-image:fpm-latest

RUN apt-get update \
	&& apt-get install --no-install-recommends --no-install-suggests -y \
        ca-certificates \
        nginx \
        gettext-base \
        supervisor \
	&& rm -rf /var/lib/apt/lists/*


RUN cp /build/config/nginx.conf /etc/nginx/nginx.conf
RUN cp /build/config/fastcgi.conf /etc/nginx/fastcgi.conf
RUN cp /build/config/nginx-server.conf /etc/nginx/server.conf



# Make sure to reload nginx, not sure if this can be omitted
RUN cp /build/config/supervisord.conf /etc/supervisord.conf

EXPOSE 80

# Run supervisor (FPM & Nginx)
CMD ["/usr/bin/supervisord", "-n"]
