FROM registry.gitlab.com/hareland/php-image:nginx-latest


RUN cp /build/config/supervisord.conf /etc/supervisord.conf

# .. Anything from the image you extend this from

# Run supervisor (FPM & Nginx)
CMD ["/usr/bin/supervisord", "-n"]
